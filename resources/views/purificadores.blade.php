@extends('layouts.master')

@section('content')
    


<div class="container px-5 my-5 py-5" id="filters">


    <p class="text-center hero-title-blue my-5">LAMPARAS DE LUZ ULTRA VIOLETA:</p>


            {{-- <h3 class="text-center mb-0 filter-title">Emisores UVC Serie DE</h3> --}}

       <div class="row">
           <div class="col-md-6 offset-md-3 text-center mb-5">
               <img src="{{asset('/img/lamparas/lamp2.jpeg')}}" alt="">
           </div>
           <ul>
               <li>
                    El producto UVC más comúnmente usado para sistemas HVAC comerciales- el estándar industrial
               </li>
               <li>
                Ideal para sistemas  manejadores de aire medianos y grandes al reducir mantenimiento, enfermedades contagiosas y el Control de la Calidad del Aire Interior en edificios comerciales, industriales y hospitales
               </li>
           </ul>
       </div>
       
       <h3 class="text-center my-5 filter-title">TITULO</h3>
       <div class="row">
           <div class="col-md-6 offset-md-3 text-center mb-5">
                <img src="{{asset('/img/lamparas/lamp3.jpeg')}}" alt="">
           </div>

           <ul>
                <li>
                    Utilizadas para la recirculación o filtración por extracción
                </li>
                <li>
                    Remueve eficientemente los micro contaminantes aerotransportados
                </li>
                <li>
                    Excelente elección para áreas tales como de aislamiento y salas de examen, áreas de espera y salas de procedimientos
                </li>
                <li>
                    Extracción con filtración para control de presión
                </li>
            </ul>
        </div>

        <h3 class="text-center my-5 filter-title">TITULO</h3>
        <div class="row">
            <div class="col-md-6 offset-md-3 text-center mb-5">
                 <img src="{{asset('/img/lamparas/lamp5.jpeg')}}" alt="">
            </div>
 
            <ul>
                 <li>
                     Utilizadas para la recirculación o filtración por extracción
                 </li>
                 <li>
                     Remueve eficientemente los micro contaminantes aerotransportados
                 </li>
                 <li>
                     Excelente elección para áreas tales como de aislamiento y salas de examen, áreas de espera y salas de procedimientos
                 </li>
                 <li>
                     Extracción con filtración para control de presión
                 </li>
             </ul>
         </div>

         <h3 class="text-center my-5 filter-title">TITULO</h3>
         <div class="row">
             <div class="col-md-6 offset-md-3 text-center mb-5">
                  <img src="{{asset('/img/lamparas/lamp1.jpeg')}}" alt="">
             </div>
  
             <ul>
                  <li>
                      Utilizadas para la recirculación o filtración por extracción
                  </li>
                  <li>
                      Remueve eficientemente los micro contaminantes aerotransportados
                  </li>
                  <li>
                      Excelente elección para áreas tales como de aislamiento y salas de examen, áreas de espera y salas de procedimientos
                  </li>
                  <li>
                      Extracción con filtración para control de presión
                  </li>
              </ul>
          </div>


          <h3 class="text-center my-5 filter-title">TITULO</h3>
          <div class="row">
              <div class="col-md-6 offset-md-3 text-center mb-5">
                   <img src="{{asset('/img/lamparas/lamp4.jpeg')}}" alt="">
              </div>
   
              <ul>
                   <li>
                       Utilizadas para la recirculación o filtración por extracción
                   </li>
                   <li>
                       Remueve eficientemente los micro contaminantes aerotransportados
                   </li>
                   <li>
                       Excelente elección para áreas tales como de aislamiento y salas de examen, áreas de espera y salas de procedimientos
                   </li>
                   <li>
                       Extracción con filtración para control de presión
                   </li>
               </ul>
           </div>


</div>

<div class="container px-5 my-5 py-5" id="filters">


    <p class="text-center hero-title-blue my-5">PURIFICAFORES DE AIRE ULTRA VIOLETA:</p>


            <h3 class="text-center mb-0 filter-title">Emisores UVC Serie DE</h3>

       <div class="row">
           <div class="col-md-6 offset-md-3 text-center">
               <img src="{{asset('/img/filtros/UV/DE1.png')}}" alt="">
           </div>
           <ul>
               <li>
                    El producto UVC más comúnmente usado para sistemas HVAC comerciales- el estándar industrial
               </li>
               <li>
                Ideal para sistemas  manejadores de aire medianos y grandes al reducir mantenimiento, enfermedades contagiosas y el Control de la Calidad del Aire Interior en edificios comerciales, industriales y hospitales
               </li>
           </ul>
       </div>
       
       <h3 class="text-center my-5 filter-title">AeroMed 350C (Plafón)</h3>
       <div class="row">
           <div class="col-md-6 offset-md-3 text-center">
               <img src="{{asset('img/filtros/UV/aeromed350c.jpg')}}" alt="">
           </div>

           <ul>
                <li>
                    Utilizadas para la recirculación o filtración por extracción
                </li>
                <li>
                    Remueve eficientemente los micro contaminantes aerotransportados
                </li>
                <li>
                    Excelente elección para áreas tales como de aislamiento y salas de examen, áreas de espera y salas de procedimientos
                </li>
                <li>
                    Extracción con filtración para control de presión
                </li>
            </ul>
        </div>


</div>



<p class="marcas-title text-center">MARCAS:</p>
<div class="brands my-5">
    <div>
        <img src="{{asset('img/logos/carrier.png')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/gree.png')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/lennox.png')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/lg.png')}}" alt="" class="mx-auto">
    </div>
   
    <div>
        <img src="{{asset('img/logos/mitsubishi.png')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/rheem.png')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/trane.png')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/york.png')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/prime.webp')}}" alt="" class="mx-auto">
    </div>
    <div>
        <img src="{{asset('img/logos/mirage.png')}}" alt="" class="mx-auto mirage">
    </div>
</div>

@endsection