@extends('layouts.master')

@section('content')

<div class="container mt-5" id="aire-acondicionado">
    <p class="text-center hero-title-blue">AIRE ACONDICIONADO</p>

    <div class="row">

        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/aire_acondicionado/img_1.jpg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_1.jpg')}}" alt="">
            </a>
        </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/aire_acondicionado/img_2.jpg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_2.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/aire_acondicionado/img_3.jpg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_3.jpg')}}" alt="">
            </a>
        </div>
        
      </div>
      

      <div class="row mt-5">
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/aire_acondicionado/img_4.jpg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_4.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/aire_acondicionado/img_5.jpg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_5.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
              <a href="{{asset('img/proyectos/aire_acondicionado/img_6.jpeg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_6.jpeg')}}" alt="">
            </a>
          </div>
    </div>
      
      
      <div class="row my-5">
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/aire_acondicionado/img_7.jpeg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_7.jpeg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/aire_acondicionado/img_8.jpeg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_8.jpeg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/aire_acondicionado/img_9.jpeg')}}" data-lightbox="aire_acondicionado">
                <img src="{{asset('img/proyectos/aire_acondicionado/img_9.jpeg')}}" alt="">
            </a>
        </div>
    </div>
  --------

  <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_10.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_10.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_11.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_11.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_12.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_12.jpeg')}}" alt="">
          </a>
      </div>
  </div>


  <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_13.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_13.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_14.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_14.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_15.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_15.jpeg')}}" alt="">
          </a>
      </div>
  </div>


  <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_16.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_16.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_17.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_17.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_18.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_18.jpeg')}}" alt="">
          </a>
      </div>
  </div>

  <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_19.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_19.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_20.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_20.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_21.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_21.jpeg')}}" alt="">
          </a>
      </div>
  </div>

  <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_22.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_22.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_23.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_23.jpeg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/aire_acondicionado/img_24.jpeg')}}" data-lightbox="aire_acondicionado">
              <img src="{{asset('img/proyectos/aire_acondicionado/img_24.jpeg')}}" alt="">
          </a>
      </div>
  </div>



</div>

<div class="container mt-5" id="camaras-frias">
    <p class="text-center hero-title-blue">CAMARAS FRIAS</p>

    <div class="row">

        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/camaras_frias/img_1.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_1.jpg')}}" alt="">
            </a>
        </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/camaras_frias/img_2.png')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_2.png')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/camaras_frias/img_3.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_3.jpg')}}" alt="">
            </a>
        </div>
        
      </div>
      

      <div class="row mt-5">
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/camaras_frias/img_4.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_4.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/camaras_frias/img_5.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_5.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
              <a href="{{asset('img/proyectos/camaras_frias/img_6.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_6.jpg')}}" alt="">
            </a>
          </div>
    </div>
      
      
      <div class="row my-5">
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/camaras_frias/img_7.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_7.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/camaras_frias/img_8.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_8.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/camaras_frias/img_9.jpg')}}" data-lightbox="camaras_frias">
                <img src="{{asset('img/proyectos/camaras_frias/img_9.jpg')}}" alt="">
            </a>
        </div>
    </div>



  <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/camaras_frias/img_10.jpg')}}" data-lightbox="camaras_frias">
              <img src="{{asset('img/proyectos/camaras_frias/img_10.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/camaras_frias/img_11.jpg')}}" data-lightbox="camaras_frias">
              <img src="{{asset('img/proyectos/camaras_frias/img_11.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/camaras_frias/img_12.jpg')}}" data-lightbox="camaras_frias">
              <img src="{{asset('img/proyectos/camaras_frias/img_12.jpg')}}" alt="">
          </a>
      </div>
  </div>

  <div class="row my-5">
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/camaras_frias/img_13.jpg')}}" data-lightbox="camaras_frias">
            <img src="{{asset('img/proyectos/camaras_frias/img_13.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/camaras_frias/img_14.jpg')}}" data-lightbox="camaras_frias">
            <img src="{{asset('img/proyectos/camaras_frias/img_14.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/camaras_frias/img_15.jpg')}}" data-lightbox="camaras_frias">
            <img src="{{asset('img/proyectos/camaras_frias/img_15.jpg')}}" alt="">
        </a>
    </div>
  </div>
  
  <div class="row my-5">
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/camaras_frias/img_16.jpg')}}" data-lightbox="camaras_frias">
            <img src="{{asset('img/proyectos/camaras_frias/img_16.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/camaras_frias/img_17.jpg')}}" data-lightbox="camaras_frias">
            <img src="{{asset('img/proyectos/camaras_frias/img_17.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/camaras_frias/img_18.jpg')}}" data-lightbox="camaras_frias">
            <img src="{{asset('img/proyectos/camaras_frias/img_18.jpg')}}" alt="">
        </a>
    </div>
</div>



</div>


<div class="container mt-5" id="campanas">
    <p class="text-center hero-title-blue">CAMPANAS</p>

    <div class="row">

        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/campanas/img_1.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_1.jpg')}}" alt="">
            </a>
        </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/campanas/img_2.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_2.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/campanas/img_3.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_3.jpg')}}" alt="">
            </a>
        </div>
        
      </div>
      

      <div class="row mt-5">
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/campanas/img_4.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_4.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/campanas/img_5.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_5.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
              <a href="{{asset('img/proyectos/campanas/img_6.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_6.jpg')}}" alt="">
            </a>
          </div>
    </div>
      
      
      <div class="row my-5">
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/campanas/img_7.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_7.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/campanas/img_8.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_8.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/campanas/img_9.jpg')}}" data-lightbox="campanas">
                <img src="{{asset('img/proyectos/campanas/img_9.jpg')}}" alt="">
            </a>
        </div>
    </div>
  
    <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/campanas/img_10.jpg')}}" data-lightbox="campanas">
              <img src="{{asset('img/proyectos/campanas/img_10.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/campanas/img_11.jpg')}}" data-lightbox="campanas">
              <img src="{{asset('img/proyectos/campanas/img_11.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/campanas/img_12.jpg')}}" data-lightbox="campanas">
              <img src="{{asset('img/proyectos/campanas/img_12.jpg')}}" alt="">
          </a>
      </div>
  </div>

  <div class="row my-5">
    <div class="col-md-4 text-center proyectos">
         <a href="{{asset('img/proyectos/campanas/img_13.jpg')}}" data-lightbox="campanas">
            <img src="{{asset('img/proyectos/campanas/img_13.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
         <a href="{{asset('img/proyectos/campanas/img_14.jpg')}}" data-lightbox="campanas">
            <img src="{{asset('img/proyectos/campanas/img_14.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
         <a href="{{asset('img/proyectos/campanas/img_15.jpg')}}" data-lightbox="campanas">
            <img src="{{asset('img/proyectos/campanas/img_15.jpg')}}" alt="">
        </a>
    </div>
</div>

<div class="row my-5">
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/campanas/img_16.jpg')}}" data-lightbox="campanas">
          <img src="{{asset('img/proyectos/campanas/img_16.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
   
    </div>
  <div class="col-md-4 text-center proyectos">

  </div>
</div>


</div>



<div class="container mt-5" id="ducterias">
    <p class="text-center hero-title-blue">DUCTERIAS</p>

    <div class="row">

        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/ducterias/img_1.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_1.jpg')}}" alt="">
            </a>
        </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/ducterias/img_2.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_2.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/ducterias/img_3.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_3.jpg')}}" alt="">
            </a>
        </div>
        
      </div>
      

      <div class="row mt-5">
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/ducterias/img_4.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_4.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/ducterias/img_5.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_5.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
              <a href="{{asset('img/proyectos/ducterias/img_6.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_6.jpg')}}" alt="">
            </a>
          </div>
    </div>
      
      
      <div class="row my-5">
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/ducterias/img_7.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_7.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/ducterias/img_8.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_8.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/ducterias/img_9.jpg')}}" data-lightbox="ducterias">
                <img src="{{asset('img/proyectos/ducterias/img_9.jpg')}}" alt="">
            </a>
        </div>
    </div>

    <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/ducterias/img_10.jpg')}}" data-lightbox="ducterias">
              <img src="{{asset('img/proyectos/ducterias/img_10.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/ducterias/img_11.jpg')}}" data-lightbox="ducterias">
              <img src="{{asset('img/proyectos/ducterias/img_11.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/ducterias/img_12.jpg')}}" data-lightbox="ducterias">
              <img src="{{asset('img/proyectos/ducterias/img_12.jpg')}}" alt="">
          </a>
      </div>
  </div>


  <div class="row my-5">
    <div class="col-md-4 text-center proyectos">
         <a href="{{asset('img/proyectos/ducterias/img_13.jpg')}}" data-lightbox="ducterias">
            <img src="{{asset('img/proyectos/ducterias/img_13.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
         <a href="{{asset('img/proyectos/ducterias/img_14.jpg')}}" data-lightbox="ducterias">
            <img src="{{asset('img/proyectos/ducterias/img_14.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
         <a href="{{asset('img/proyectos/ducterias/img_15.jpg')}}" data-lightbox="ducterias">
            <img src="{{asset('img/proyectos/ducterias/img_15.jpg')}}" alt="">
        </a>
    </div>
</div>


<div class="row my-5">
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_16.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_16.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_17.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_17.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_18.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_18.jpg')}}" alt="">
      </a>
  </div>
</div>


<div class="row my-5">
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_19.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_19.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_20.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_20.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_21.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_21.jpg')}}" alt="">
      </a>
  </div>
</div>


<div class="row my-5">
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_22.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_22.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_23.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_23.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_24.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_24.jpg')}}" alt="">
      </a>
  </div>
</div>


<div class="row my-5">
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_25.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_25.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_26.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_26.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_27.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_27.jpg')}}" alt="">
      </a>
  </div>
</div>


<div class="row my-5">
  <div class="col-md-4 text-center proyectos">
       <a href="{{asset('img/proyectos/ducterias/img_28.jpg')}}" data-lightbox="ducterias">
          <img src="{{asset('img/proyectos/ducterias/img_28.jpg')}}" alt="">
      </a>
    </div>
  <div class="col-md-4 text-center proyectos">
  
    </div>
  <div class="col-md-4 text-center proyectos">
     
  </div>
</div>
  


</div>

<div class="container mt-5" id="extraccion-ventilacion">
    <p class="text-center hero-title-blue">EXTRACCIÓN Y VENTILACIÓN</p>

    <div class="row">

        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/extraccion_ventilacion/img_1.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_1.jpg')}}" alt="">
            </a>
        </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/extraccion_ventilacion/img_2.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_2.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/extraccion_ventilacion/img_3.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_3.jpg')}}" alt="">
            </a>
        </div>
        
      </div>
      

      <div class="row mt-5">
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/extraccion_ventilacion/img_4.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_4.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
            <a href="{{asset('img/proyectos/extraccion_ventilacion/img_5.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_5.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
              <a href="{{asset('img/proyectos/extraccion_ventilacion/img_6.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_6.jpg')}}" alt="">
            </a>
          </div>
    </div>
      
      
      <div class="row my-5">
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/extraccion_ventilacion/img_7.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_7.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/extraccion_ventilacion/img_8.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_8.jpg')}}" alt="">
            </a>
          </div>
        <div class="col-md-4 text-center proyectos">
             <a href="{{asset('img/proyectos/extraccion_ventilacion/img_9.jpg')}}" data-lightbox="extraccion_ventilacion">
                <img src="{{asset('img/proyectos/extraccion_ventilacion/img_9.jpg')}}" alt="">
            </a>
        </div>
    </div>

    <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/extraccion_ventilacion/img_10.jpg')}}" data-lightbox="extraccion_ventilacion">
              <img src="{{asset('img/proyectos/extraccion_ventilacion/img_10.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/extraccion_ventilacion/img_11.jpg')}}" data-lightbox="extraccion_ventilacion">
              <img src="{{asset('img/proyectos/extraccion_ventilacion/img_11.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
           <a href="{{asset('img/proyectos/extraccion_ventilacion/img_12.jpg')}}" data-lightbox="extraccion_ventilacion">
              <img src="{{asset('img/proyectos/extraccion_ventilacion/img_12.jpg')}}" alt="">
          </a>
      </div>
  </div>

    <div class="row my-5">
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/extraccion_ventilacion/img_13.jpg')}}" data-lightbox="extraccion_ventilacion">
              <img src="{{asset('img/proyectos/extraccion_ventilacion/img_13.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/extraccion_ventilacion/img_14.jpg')}}" data-lightbox="extraccion_ventilacion">
              <img src="{{asset('img/proyectos/extraccion_ventilacion/img_14.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center proyectos">
          <a href="{{asset('img/proyectos/extraccion_ventilacion/img_15.jpg')}}" data-lightbox="extraccion_ventilacion">
              <img src="{{asset('img/proyectos/extraccion_ventilacion/img_15.jpg')}}" alt="">
          </a>
      </div>
  </div>
  
  <div class="row my-5">
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/extraccion_ventilacion/img_16.jpg')}}" data-lightbox="extraccion_ventilacion">
            <img src="{{asset('img/proyectos/extraccion_ventilacion/img_16.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/extraccion_ventilacion/img_17.jpg')}}" data-lightbox="extraccion_ventilacion">
            <img src="{{asset('img/proyectos/extraccion_ventilacion/img_17.jpg')}}" alt="">
        </a>
      </div>
    <div class="col-md-4 text-center proyectos">
        <a href="{{asset('img/proyectos/extraccion_ventilacion/img_18.jpg')}}" data-lightbox="extraccion_ventilacion">
            <img src="{{asset('img/proyectos/extraccion_ventilacion/img_18.jpg')}}" alt="">
        </a>
    </div>
  </div>


</div>


<div class="container mt-5" id="extraccion-ventilacion">
  <p class="text-center hero-title-blue">FILTROS PURIFICADORES DE AIRE</p>

  <div class="row">

      <div class="col-md-4 text-center">
          <a href="{{asset('img/proyectos/filtros_purificadores/img_1.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_1.jpg')}}" alt="">
          </a>
      </div>
      <div class="col-md-4 text-center">
          <a href="{{asset('img/proyectos/filtros_purificadores/img_2.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_2.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center">
          <a href="{{asset('img/proyectos/filtros_purificadores/img_3.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_3.jpg')}}" alt="">
          </a>
      </div>
      
    </div>
    

    <div class="row mt-5">
      <div class="col-md-4 text-center">
          <a href="{{asset('img/proyectos/filtros_purificadores/img_4.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_4.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center">
          <a href="{{asset('img/proyectos/filtros_purificadores/img_5.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_5.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center">
            <a href="{{asset('img/proyectos/filtros_purificadores/img_6.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_6.jpg')}}" alt="">
          </a>
        </div>
  </div>
    
    
    <div class="row my-5">
      <div class="col-md-4 text-center">
           <a href="{{asset('img/proyectos/filtros_purificadores/img_7.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_7.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center">
           <a href="{{asset('img/proyectos/filtros_purificadores/img_8.jpg')}}" data-lightbox="filtros_purificadores">
              <img src="{{asset('img/proyectos/filtros_purificadores/img_8.jpg')}}" alt="">
          </a>
        </div>
      <div class="col-md-4 text-center">

      </div>
  </div>



</div>
@endsection